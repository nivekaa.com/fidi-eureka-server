## Fidi-Eureka-Server

#### Description
As all services are registered to the Eureka server and lookup done by calling the Eureka Server, any change in service locations need not be handled and is taken care of

Ref: https://www.javainuse.com/spring/spring_eurekaregister
